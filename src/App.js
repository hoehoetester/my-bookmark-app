import React, { Component } from 'react';
import './App.css';

import Bookmarks from './Bookmarks';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Bookmarks />
      </div>
    );
  }
}

export default App;
