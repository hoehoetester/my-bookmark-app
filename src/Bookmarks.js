import React, { Component } from 'react';
import fbconfig from './firebase.config';

const firebase = require('firebase');

firebase.initializeApp(fbconfig);
const bookmarksRef = firebase.database().ref('bookmarks');

class Bookmarks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: [],
            loggedIn: false
        };
        this.addBookmark = this.addBookmark.bind(this);
        this.deleteBookmark = this.deleteBookmark.bind(this);
        this.login = this.login.bind(this);
    }

    componentWillMount() {
        this.getAll();

        bookmarksRef.on('child_added', snapshot => {
            const m = snapshot.val();
            let urls = this.state.url;

            urls.push({
                id: snapshot.key,
                'title': m.post.title,
                'url': m.post.url,
                isJa: m.post.isJa
            });

            this.setState({ url: urls });
        });
    }

    getAll() {
        console.clear();

        bookmarksRef.once('value')
            .then((snapshot) => {
                var posts = [];
                debugger;
                snapshot.forEach(child => {
                    debugger;
                    var bookmark = {
                        id: child.key,
                        title: child.val().post.title,
                        url: child.val().post.url,
                        isJa: child.val().post.isJa
                    };
                    posts.push(bookmark);

                });

                this.setState({
                    url: posts
                });
            });
    }

    addBookmark(e) {
        e.preventDefault();
        let newBookmark = {
            title: this.refs.title.value,
            url: this.refs.url.value,
            isJa: this.refs.isJa.checked
        };

        bookmarksRef.push({
            post: newBookmark
        }, error => {
            if (error) {
                console.log('Error has occured during saving process');
            } else {
                console.log("Data has been saved succesfully");
            }
        });

        this.refs.title.value = "";
        this.refs.url.value = "";
        this.refs.isJa.checked = false;
    }

    deleteBookmark(item) {
        let result = window.confirm('are you sure?');
        if (result) {
            if (item.id) {
                bookmarksRef.child(item.id).remove(res => {
                    this.getAll();
                });
            } else {
                bookmarksRef.remove(res => {
                    this.getAll();
                });
            }
        }
    }

    login() {
        let email = this.refs.email.value;
        let password = this.refs.password.value;
        debugger;
        // if (email && password && password === 'tttt') {
        //     this.setState({ loggedIn: true });
        // }


        // const auth = firebase.auth();
        // const promise = auth.signInWithEmailAndPassword(email, password);

        let provider = new firebase.auth.GoogleAuthProvider();
        let promise = firebase.auth().signInWithPopup(provider);


        promise
            .then(user => {
                debugger;
                this.setState({ loggedIn: true });
            })
            .catch(e => {
                debugger;
                let err = e.message;
                console.log(err);
            }

            );



    }


    render() {
        let _data = this.state.url;

        return (
            <div>
                {this.state.loggedIn ? '' :
                    <div id="login">
                        <div className="foo">
                            <div>please login</div><br />
                            <input type="email" ref="email" placeholder="email" /><br />
                            <input type="password" ref="password" placeholder="password" /><br />
                            <button className="t-btn" onClick={this.login}>login</button>
                        </div>
                    </div>
                }

                <div>
                    <h4 className="t-header-title">my bookmarks fb</h4>
                    <form onSubmit={this.addBookmark}>
                        <input type="text" ref="title" placeholder="title" />
                        <br />
                        <input type="url" ref="url" placeholder="url" required />
                        <br />

                        <label className="t-checkbox">
                            <input type="checkbox" ref="isJa" /><span>isJa</span>
                        </label>
                        <br />
                        <button type="submit" className="t-btn">add</button>
                    </form>
                    <button onClick={this.deleteBookmark} className="t-btn">delete all</button>

                    <ul>
                        {_data.map((url, i) => {
                            return (
                                <li key={i}>
                                    <span
                                        ref="del"
                                        className="t-delete"
                                        onClick={this.deleteBookmark.bind(this, url)}
                                    >
                                        +
                                </span>
                                    <span className="t-name">{url.title}</span>
                                    <span className="t-badge">{Boolean(url.isJa) ? "ja" : ""}</span>
                                    <a href={url.url} className="t-link" target="_blank">
                                        {url.url}
                                    </a>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}

export default Bookmarks;